import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // TODO: En este lado ira todoa la conexion con la api para recibir los servicios
  private url = 'https://localhost/auth_app/api';
  constructor(private http: HttpClient) { }
}
